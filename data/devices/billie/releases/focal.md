---
kernelVersion: "4.19.81"
portType: "Halium 10.0"
installLink: "https://gitlab.com/ubports/porting/community-ports/android10/oneplus-nord-n10/oneplus-nord-n10/-/blob/android-10/README.md?ref_type=heads"

portStatus:
  - categoryName: "Actors"
    features:
      - id: "manualBrightness"
        value: "+"
      - id: "notificationLed"
        value: "x"
      - id: "torchlight"
        value: "+"
      - id: "vibration"
        value: "+"
  - categoryName: "Camera"
    features:
      - id: "flashlight"
        value: "+"
      - id: "photo"
        value: "+"
      - id: "video"
        value: "+"
      - id: "switchCamera"
        value: "+"
  - categoryName: "Cellular"
    features:
      - id: "carrierInfo"
        value: "+"
      - id: "dataConnection"
        value: "+"
      - id: "dualSim"
        value: "+"
      - id: "calls"
        value: "+"
      - id: "mms"
        value: "+"
      - id: "pinUnlock"
        value: "+"
      - id: "sms"
        value: "+"
      - id: "audioRoutings"
        value: "+"
      - id: "voiceCall"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "Endurance"
    features:
      - id: "batteryLifetimeTest"
        value: "+"
      - id: "noRebootTest"
        value: "+"
  - categoryName: "GPU"
    features:
      - id: "uiBoot"
        value: "+"
      - id: "videoAcceleration"
        value: "+"
  - categoryName: "Misc"
    features:
      - id: "apparmorPatches"
        value: "+"
      - id: "batteryPercentage"
        value: "+"
      - id: "offlineCharging"
        value: "+"
      - id: "onlineCharging"
        value: "+"
      - id: "recoveryImage"
        value: "+"
      - id: "factoryReset"
        value: "+"
      - id: "sdCard"
        value: "+"
      - id: "rtcTime"
        value: "+"
      - id: "shutdown"
        value: "+"
      - id: "wirelessCharging"
        value: "x"
      - id: "wirelessExternalMonitor"
        value: "+"
      - id: "waydroid"
        value: "+"
  - categoryName: "Network"
    features:
      - id: "bluetooth"
        value: "+"
      - id: "flightMode"
        value: "+"
      - id: "hotspot"
        value: "+"
        overrideGlobal: true
      - id: "nfc"
        value: "+"
      - id: "wifi"
        value: "+"
      - id: "fmRadio"
        value: "+"
  - categoryName: "Sensors"
    features:
      - id: "autoBrightness"
        value: "+"
      - id: "fingerprint"
        value: "+"
      - id: "gps"
        value: "+"
      - id: "proximity"
        value: "+"
      - id: "rotation"
        value: "+"
      - id: "touchscreen"
        value: "+"
      - id: "dt2w"
        value: "x"
  - categoryName: "Sound"
    features:
      - id: "earphones"
        value: "+"
      - id: "loudspeaker"
        value: "+"
      - id: "microphone"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "USB"
    features:
      - id: "mtp"
        value: "+"
      - id: "adb"
        value: "+"
      - id: "wiredExternalMonitor"
        value: "x"
---

<section id="preinstall">

- Use Oneplus Unbrick tools requires phone to be in EDL mode. You should use either the EU firmware [OxygenOS_10.5.7](https://onepluscommunityserver.com/list/Unbrick_Tools/OnePlus_Nord_N10_5G/EU_BE89BA/Q/OnePlus_Nord_N10_5G_EU_OxygenOS_10.5.7.zip) or the Global [OxygenOS_10.5.7](https://onepluscommunityserver.com/list/Unbrick_Tools/OnePlus_Nord_N10_5G/Global_BE86AA/Q/OnePlus_Nord_N10_5G_Global_OxygenOS_10.5.7.zip) before installing.

</section>
