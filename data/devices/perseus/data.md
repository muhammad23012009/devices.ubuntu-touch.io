---
name: "Xiaomi Mi MIX 3"
comment: "community device"
deviceType: "phone"

deviceInfo:
  - id: "cpu"
    value: "Octa-core (4x2.8 GHz Kryo 385 Gold & 4x1.7 GHz Kryo 385 Silver)"
  - id: "chipset"
    value: "Qualcomm SDM845 Snapdragon 845"
  - id: "gpu"
    value: "Adreno 630"
  - id: "rom"
    value: "128 GB, 256 GB"
  - id: "ram"
    value: "6 GB, 8 GB, 10GB"
  - id: "android"
    value: "Android 9.0 (Pie), upgradable to Android 10, MIUI 12"
  - id: "battery"
    value: "Li-Po 3200 mAh, non-removable"
  - id: "display"
    value: "6.39 inches, 100.2 cm2 (~85.0% screen-to-body ratio), 1080 x 2340 pixels, 19.5:9 ratio (~403 ppi density)"
  - id: "rearCamera"
    value: "12 MP (wide), f/1.8, 1/2.55 in, 1.4µm, Dual Pixel PDAF, 4-axis OIS, 12 MP (telephoto), 1/3.4 in, 1.0µm"
  - id: "frontCamera"
    value: "Manual pop-up 24 MP, f/2.0, 26mm (wide), 1/2.8 in, 0.9µm, Manual pop-up 2 MP, depth sensor"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "157.9 x 74.7 x 8.5 mm (6.22 x 2.94 x 0.33 in)"
  - id: "weight"
    value: "218 g (7.69 oz)"

contributors:
  - name: "Perseus X"
    photo: "https://forums.ubports.com/assets/uploads/profile/uid-7027/7027-profileavatar-1625547143025.png"
    forum: "https://forums.ubports.com/user/xperseus"

sources:
  portType: "community"
  portPath: "android9"
  deviceGroup: "xiaomi-mi-mix-3"
  deviceSource: "xiaomi-perseus"
  kernelSource: "kernel-xiaomi-perseus"
  showDevicePipelines: true

communityHelp:
  - name: "Telegram Group - Mi MIX 3 Global Community"
    link: "https://t.me/MiMix3Global"
---
