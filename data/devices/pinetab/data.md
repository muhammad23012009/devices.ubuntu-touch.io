---
name: "PineTab"
deviceType: "tablet"
image: "https://gitlab.com/design3680819/ut-hw-models/-/raw/main/Tablets/PineTab/PineTab.png"
buyLink: "https://pine64.com/"
price:
  avg: 120
  currency: "USD"
subforum: "87/pine64"
description: "The PineTab is a mainline Linux tablet created by PINE64. The optional keyboard and trackpad – which doubles-up as a screen cover – effectively converts the PineTab into a petite on-the-go laptop with a touch screen functionality."

deviceInfo:
  - id: "cpu"
    value: "4x 1152 MHz Cortex-A53"
  - id: "chipset"
    value: "Allwinner A64"
  - id: "gpu"
    value: "Mali-400 MP2"
  - id: "rom"
    value: "64 GB"
  - id: "ram"
    value: "2 GB"
  - id: "battery"
    value: "6000 mAh"
  - id: "display"
    value: '10.1" 1280×800 IPS'
  - id: "rearCamera"
    value: "Single 5MP, 1/4″, LED Flash"
  - id: "frontCamera"
    value: "Single 2MP, f/2.8, 1/5″"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "258mm x 170mm x 11.2mm"
  - id: "weight"
    value: "575 grams"
  - id: "releaseDate"
    value: "05.09.2020"

communityHelp:
  - name: "Pine64 subforum on UBports Forum"
    link: "https://forums.ubports.com/category/87/pine64"
  - name: "Pinetab subforum on Pine64 Forum"
    link: "https://forum.pine64.org/forumdisplay.php?fid=142"
  - name: "PineTab subreddit"
    link: "https://www.reddit.com/r/PineTab/"
  - name: "IRC ( #pinetab on irc.pine64.org )"
    link: "https://www.pine64.org/web-irc/"
  - name: "#pinetab on Pine64 Discord"
    link: "https://discord.gg/knRjbFe2jP"
  - name: "Telegram chat"
    link: "https://t.me/utonpine"

contributors:
  - name: "Pine64"
    role: "Phone maker"
  - name: "Oren Klopfer"
    role: "Developer"
---
