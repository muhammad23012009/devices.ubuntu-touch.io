---
name: "Desktop PC"
comment: "experimental"
deviceType: "tv"
image: "https://unity8.io/img.png"
disableBuyLink: true
description: "You can install Lomiri, the desktop environment of Ubuntu Touch previously known as Unity 8, on all debian-derived Linux distributions, including Ubuntu."
---
