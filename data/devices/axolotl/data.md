---
name: "SHIFT6mq (axolotl)"
deviceType: "phone"
description: "THE #LOVEPHONE - As a small family business without investors we have a clear goal: To do as much good and as little damage as possible. We take this goal very seriously and implement it consequently and in an optimized way. We don’t extract personal profits and the budget for advertisement and marketing is smaller than 0.1 percent."
image: "https://raw.githubusercontent.com/SHIFTPHONES/android_device_shift_axolotl/metadata/art/SHIFT6mq.jpg"
subforum: "109/shift-6mq"
price:
  avg: 633
  currency: "EUR"
  currencySymbol: "€"

deviceInfo:
  - id: "releaseDate"
    value: "01.01.2021"
  - id: "cpu"
    value: "Octa-core Kryo 385 (4 x 2.8 GHz + 4 x 1.7 GHz)"
  - id: "chipset"
    value: "Qualcomm SDM845 Snapdragon 845"
  - id: "gpu"
    value: "Adreno 630"
  - id: "rom"
    value: "128 GB UFS 2.1"
  - id: "ram"
    value: "8 GB LPDDR4X"
  - id: "android"
    value: "10.0 (Q)"
  - id: "battery"
    value: "3850 mAh"
  - id: "display"
    value: "152.4 mm (6 in) : 2160x1080 (402 PPI) - AMOLED"
  - id: "rearCamera"
    value: "16 MP (IMX519), 24 MP (S5K2X7SP), Dual LED (dual tone) flash"
  - id: "frontCamera"
    value: "16 MP (S5K3P9SP04-FGX9), No flash"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "159.8 mm (6.29 in) (h), 76.4 mm (3 in) (w), 8.5 mm (0.33 in) (d)"
  - id: "weight"
    value: "199 g"

sources:
  portType: "community"
  portPath: "android10"
  deviceGroup: "shift"
  deviceSource: "android_device_shift_axolotl"
  kernelSource: "android_kernel_shift_axolotl"

contributors:
  - name: "SHIFT GmbH"
    role: "Phone maker"
  - name: "amartinz"
    forum: "https://forums.ubports.com/user/amartinz"
  - name: "Flohack"
    forum: "https://forums.ubports.com/user/Flohack"
    photo: "https://forums.ubports.com/assets/uploads/profile/uid-414/414-profileavatar.png"

seo:
  description: "Switch your SHIFT6mq to Ubuntu Touch, as your open source daily driver OS."
  keywords: "Ubuntu Touch, shiftphones, SHIFT6mq, Linux on Mobile"
---
