---
name: "Google Nexus 6P"
deviceType: "phone"
subforum: "57/google-nexus-6p"

contributors:
  - name: "Flohack"
    forum: "https://forums.ubports.com/user/Flohack"
    photo: "https://forums.ubports.com/assets/uploads/profile/uid-414/414-profileavatar.png"

seo:
  description: "Switch your Nexus 6P smartphone OS to Ubuntu Touch, as your main daily driver operating system."
  keywords: "Ubuntu Touch, Nexus 6P, linux for smartphone, Linux on phone"
---
