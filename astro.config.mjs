import { defineConfig } from "astro/config";
import sitemap from "@astrojs/sitemap";

// https://astro.build/config
export default defineConfig({
  site: "https://devices.ubuntu-touch.io/",
  build: {
    inlineStylesheets: "never"
  },
  image: {
    remotePatterns: [{ protocol: "https" }, { protocol: "http" }]
  },
  integrations: [
    {
      name: "astro-netlify-redirects",
      hooks: {
        "astro:config:setup": ({ injectRoute }) => {
          injectRoute({
            pattern: "/_redirects",
            entrypoint: "./src/pages/_redirects.js"
          });
        }
      }
    },
    sitemap({
      serialize(item) {
        if (/^.*\/device\/[^\/]*\/?$/.test(item.url)) {
          item.changefreq = "weekly";
          item.priority = 0.7;
        }

        return item;
      }
    })
  ]
});
