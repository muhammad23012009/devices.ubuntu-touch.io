import Ajv from "ajv";
import addFormats from "ajv-formats";
import rulesGenerator from "@data/validationRules.js";
import { portStatusAsObject, deviceInfoAsObject } from "./dataUtils.js";

import * as path from "path";
import pMemoize from "p-memoize";

const rules = rulesGenerator();
const ajv = new Ajv({ strict: "log", allErrors: true });
addFormats(ajv);
const validate = ajv.compile(rules);

const ansiCodes = {
  yellowFG: "\x1b[33m",
  redFG: "\x1b[31m",
  greenFG: "\x1b[32m",
  reset: "\x1b[0m"
};

async function runTests(memoCode, device) {
  let ciDiff = [];

  try {
    ciDiff = process.env.CI_FILES_CHANGED.split("\n").map((e) => {
      return path.basename(e, device.fileInfo.extension);
    });
  } catch (e) {}

  if (
    !process.argv.includes("test-changes") ||
    ciDiff.includes(device.codename)
  ) {
    let validatedDevice = JSON.parse(JSON.stringify(device));
    validatedDevice.portStatus = portStatusAsObject(validatedDevice.portStatus);
    validatedDevice.deviceInfo = deviceInfoAsObject(validatedDevice.deviceInfo);

    const notValid = validate(validatedDevice) ? [] : validate.errors;
    device.notValid = notValid.filter((err) => err.keyword != "if");

    return device;
  }
}

export function printTests(device) {
  console.log(
    device.notValid.length
      ? ansiCodes.yellowFG + "%s" + ansiCodes.reset
      : ansiCodes.greenFG + "%s" + ansiCodes.reset,
    "\n[" + device.codename + "@" + device.release + "] " + device.name
  );

  device.notValid.forEach((err) =>
    console.log("    " + [err.instancePath, err.message].join(" ").trim())
  );
}

export default pMemoize(runTests);
