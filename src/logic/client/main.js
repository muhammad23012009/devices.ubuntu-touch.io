import slugify from "@sindresorhus/slugify";

import { navigate } from "astro:transitions/client";

import {
  startTracking,
  spaNavigationTrack
} from "ubports-astro-components/js/mtm-loader.js";

import DarkMode, {
  setDefaultColorMode
} from "ubports-astro-components/js/darkMode.js";
import HeaderContainer from "ubports-astro-components/js/header.js";
import ToggleButton from "ubports-astro-components/js/toggleButton.js";

import DonateBanner from "@logic/client/components/donateBanner.js";
import SidenavScrollspy from "@logic/client/components/sidenav.js";
import SearchSidebar from "@logic/client/components/search.js";

import TippyTooltip from "@logic/client/components/tooltips.js";
import SidebarJS from "@logic/client/components/sidebars.js";
import StarSelector from "@logic/client/components/starSelector.js";

export default function registerComponents() {
  // Navigation
  let correctedPageSlug = window.location.pathname
    .split("/")
    .map((folder) => slugify(folder, { decamelize: false }))
    .join("/");
  if (correctedPageSlug != window.location.pathname)
    navigate(correctedPageSlug, { history: "replace" });

  // SPA transitions
  document.addEventListener("astro:before-swap", (ev) => {
    setDefaultColorMode(ev.newDocument);
  });

  // Components
  customElements.define("donate-banner", DonateBanner);
  customElements.define("dark-mode-switch", DarkMode);

  customElements.define("header-container", HeaderContainer);
  customElements.define("toggle-button", ToggleButton);
  customElements.define("sidenav-scrollspy", SidenavScrollspy);
  customElements.define("search-sidebar", SearchSidebar);

  customElements.define("sidebarjs-component", SidebarJS);
  customElements.define("tippy-tooltip", TippyTooltip);
  customElements.define("star-selector", StarSelector);

  // Tracking code
  document.addEventListener(
    "astro:page-load",
    () => {
      startTracking("https://analytics.ubports.com/", "3");
      document.addEventListener("astro:page-load", spaNavigationTrack);
    },
    { once: true }
  );
}
